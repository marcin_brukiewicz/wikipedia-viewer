const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production'; // based on the constiable set in the terminal
const cssDev = ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'];
const cssProd = ExtractTextPlugin.extract({
  use: ['css-loader', 'postcss-loader', 'sass-loader'],
  fallback: 'style-loader'
});
const cssConfig = isProd ? cssProd : cssDev;

module.exports = {
  entry: {
    'js/vendor': ['react', 'react-dom', 'jquery'],
    'js/index': path.join(__dirname, 'src', 'index.js'),
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].bundle.js' // chunk hash is a hash genera
  },
  module: {
    loaders: [
      {
        test: /\.s?css$/,
        use: cssConfig
      },
      {
        test: /\.tsx?$/,
        use: 'awesome-typescript-loader',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        loader: 'babel-loader', // Options are defined in .babelrc
        exclude: /node_modules/,
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/i,
        use: [
          'file-loader?name=[name].[ext]&outputPath=images/', // The options passed after '?' preserve the path structure in production (so we get a e.g dist/images/background.PNG instead of just dist/background.PNG)
          'image-webpack-loader'
        ]
      },
      { test: /\.(ttf|eot)$/, loader: 'file-loader?name=fonts/[name].[ext]' },
    ]
  },
  // Read about browserSync and its webpack plugin
  devServer: {
    contentBase: path.join(__dirname, "dist"), // Server files from this folder
    compress: true, // Use gzip compression
    hot: true,
    stats: "errors-only", // Display only errors while running the webpack-dev-server
    open: false // Opens new window in the browser atutomatically after webpack-dev-server starts
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new HtmlWebpackPlugin({
      title: 'Index',
      hash: true,
      chunks: ['js/vendor', 'js/index'],
      filename: 'index.html',
      template: './src/templates/index.ejs'
    }),
    new ExtractTextPlugin({
      filename: 'css/[name].css',
      disable: !isProd, // Turning it on/off based on the environment
      allChunks: true
    }),
    new webpack.HotModuleReplacementPlugin(), // enable hot module replacement
    new webpack.NamedModulesPlugin(), // more readable module names in the browser
  ]
}
