import React from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import * as actions from '../actions/search';
import 'react-select/dist/react-select.css';
import * as API from '../services/api';

class SearchContainer extends React.Component {
  constructor(props) {
    super(props);
    this.buttonClick = this.buttonClick.bind(this);
  }
  buttonClick() {
    const { search, getPageText } = this.props;
    if (search.term == '') return;
    getPageText(search.term.value);
  }
  render() {
    const { term } = this.props.search;
    const { selectTerm } = this.props;
    console.log('Props:', this.props);
    return (
      <div className="select-wrapper flex__row">
        <Select.Async
          name="wikipedia-search"
          value={term}
          loadOptions={API.getSuggestions}
          onChange={selectTerm}
          resetValue=''
          className="flex-1"
          placeholder="Wisdom is out there..."
        />
        <button className="btn btn--primary" onClick={this.buttonClick}>View</button>
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  search: state.search
});


SearchContainer = connect(mapStateToProps, actions)(SearchContainer);

export default SearchContainer;
