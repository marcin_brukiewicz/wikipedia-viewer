import React from 'react';
import { connect } from 'react-redux';
import { DoubleBounce } from 'better-react-spinkit';
import * as actions from '../actions/search';
import { setupContentLinks } from '../actions/content';

class ContentContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { contentRevealed: false }
  }
  componentDidMount() {
    const { selectTerm, getPageText } = this.props;
    setupContentLinks(selectTerm, getPageText);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.content.value != '' && !this.state.contentRevealed) {
      $('.content-wrapper').css({ 'flex': '7' });
      $('.content').css({ 'transform': 'translateY(0vh)'});
      this.setState({ contentRevealed: true });
    }
  }
  render() {
    const { content } = this.props;
    return (
      <div className="content-wrapper">
        {content.fetching && <DoubleBounce color="#ffac00" size={50} className="loading-indicator"/>}
        <div className="content shadow--modern" dangerouslySetInnerHTML={{__html: content.value}} />
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  content: state.content
});


ContentContainer = connect(mapStateToProps, actions)(ContentContainer);

export default ContentContainer;
