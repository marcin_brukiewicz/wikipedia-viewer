import axios from 'axios';

const API = axios.create({
  baseURL: '',
});
const baseURL = 'https://en.wikipedia.org/w/api.php';
export const getSuggestions = (input) => {
  return axios.get(baseURL, {
    params: {
      action: 'opensearch',
      format: 'json',
      search: input,
      namespace: 0,
      limit: 10,
      origin: '*'
    }
  })
  .then(response => {
    const suggestions = response.data[1];
    const links = response.data[3];
    return {
      options: suggestions.map((suggestion, index) => {
        return { value: suggestion, label: suggestion, link: links[index] }
      })
    };
  })
  .catch(error => {
    console.log(error);
  });
}

export const getPageText = (title) => {
  return axios.get(baseURL, {
    params: {
      action: 'parse',
      format: 'json',
      page: title,
      origin: '*'
    }
  })
  .then(response => {
    console.log('getPageText response', response);
    return response.data.parse.text['*'];
  })
  .catch(error => {
    console.log(error);
  })
}
