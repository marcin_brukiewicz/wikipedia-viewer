import * as API from '../services/api';

export const selectTerm = (term) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'SELECT_TERM',
      term
    });
  }
}
export const getPageText = (title) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'PAGE_TEXT_REQUEST'
    });
    API.getPageText(title).then(content => {
      dispatch({
        type: 'PAGE_TEXT_SUCCESS',
        content
      })
    });
  }
}
