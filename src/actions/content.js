export const setupContentLinks = (selectTerm, getPageText) => {
  $(document).on('click', '.content a[href^=\'/wiki/\']', function (e) {
      e.preventDefault();
      const href = e.target.href;
      const title = decodeURI(href.substr(href.lastIndexOf('/') + 1));
      selectTerm(title);
      getPageText(title);
  });
}
