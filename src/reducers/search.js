const initialState = {
  term: '',
  content: '',
}
const search = (state = initialState, action) => {
  switch(action.type) {
    case 'SELECT_TERM': {
      return {
        ...state,
        term: action.term
      }
    }
    // case 'GET_PAGE_TEXT': {
    //   return {
    //     ...state,
    //     content: action.content
    //   }
    // }
    default:
      return state;
  }
}

export default search;
