const initialState = {
  fetching: false,
  value: ''
}
const search = (state = initialState, action) => {
  switch(action.type) {
    case 'PAGE_TEXT_REQUEST': {
      return {
        ...state,
        fetching: true
      }
    }
    case 'PAGE_TEXT_SUCCESS': {
      return {
        ...state,
        value: action.content,
        fetching: false
      }
    }
    default:
      return state;
  }
}

export default search;
