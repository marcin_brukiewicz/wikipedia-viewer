import { combineReducers } from 'redux';
import search from './search';
import content from './content';


const rootReducer = combineReducers({
  search,
  content
});

export default rootReducer;
