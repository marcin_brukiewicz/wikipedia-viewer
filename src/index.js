require('./styles/index.scss');
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createStore from './createStore';
import SearchContainer from './containers/SearchContainer';
import ContentContainer from './containers/ContentContainer';

const store = createStore();
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div className="container flex__column">
          <div className="search-wrapper flex__column cen-ver">
            <h1 className="text-center">Wikipedia Viewer</h1>
            <SearchContainer />
          </div>
          <ContentContainer />
        </div>
      </Provider>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
